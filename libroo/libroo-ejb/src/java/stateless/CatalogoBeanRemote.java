/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;
import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;
/**
 *
 * @author fer
 */
@Remote
public interface CatalogoBeanRemote {
         public void addLibro(java.lang.String titulo,java.lang.String autor,BigDecimal precio);
    public Collection <Libro> getAllLibros();
    public Libro buscaLibro(int id);
    public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio);
    public void eliminaLibro (int id);
}
